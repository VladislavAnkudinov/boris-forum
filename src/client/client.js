/**
 * @namespace client
 */

import { BASE_URL } from './actions/aux/actions_helpers';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Router, Route, IndexRoute, useRouterHistory } from 'react-router';
import reduxThunk from 'redux-thunk';

import { createHistory } from 'history';
const appHistory = window.APP_HISTORY = useRouterHistory(createHistory)({
  basename: BASE_URL
});

import reducers from './reducers/reducers';

import {
  AUTH_USER
} from './actions/aux/actions_types';

import App from './components/App';
import Auth from './components/Auth';
import Messages from './components/Messages';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = window.STORE = createStoreWithMiddleware(reducers);

// If we have a token, consider the user to be signed in
if (localStorage.getItem('token') && localStorage.getItem('username')) {
  // we need to update application state
  store.dispatch({type: AUTH_USER, username: localStorage.getItem('username')});
}

//browserHistory.listen(function (event) {
appHistory.listen(function (event) {
  //manage the pathname on your own
  console.log('routeChanged, pathname =', event.pathname);
  window.PATH = event.pathname;
  //store.dispatch(actionsApp.routeChanged(event.pathname));
});

ReactDOM.render(
  <Provider store={store}>
    <Router history={appHistory}>
      <Route path='/' component={App}>
        <IndexRoute component={Messages}/>
        <Route path='auth' component={Auth}/>
        <Route path='messages' component={Messages}/>
      </Route>
    </Router>
  </Provider>
  , document.querySelector('.container')
);
