import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import Toggle from 'material-ui/Toggle';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import NavigationClose from 'material-ui/svg-icons/navigation/close';

import * as actions_auth from '../actions/actions_auth'

/**
 * Main component of application
 * @memberOf client.components
 * @param {object} props - Passed parameters
 * @param {boolean} props.authenticated - If user authenticated its true
 */
class App extends Component {
  /**
   * Executed before component actually mounted
   */
  componentWillMount() {
    if (!localStorage.token) window.APP_HISTORY.push('auth');
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.authenticated && this.props.authenticated) window.APP_HISTORY.push('auth');
  }

  signout() {
    console.log('SIGNOUT!');
    this.props.authSignout();
  }

  /**
   * Renders component to HTML
   * @returns {HTML}
   */
  render() {
    return (
      <MuiThemeProvider>
        <div>
          <div>
            {this.props.username}
          </div>
          <AppBar
            title={'Boris Forum' + (window.PATH == 'auth' ? ' Auth' : '')}
            iconElementLeft={<div></div>}
            iconElementRight={localStorage.token ? <FlatButton label='Sign Out' onClick={this.signout.bind(this)} /> : <div></div>}
          />
          {this.props.children}
        </div>
      </MuiThemeProvider>
    )
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated,
    username: state.auth.username
  };
}

export default connect(mapStateToProps, {...actions_auth})(App);
