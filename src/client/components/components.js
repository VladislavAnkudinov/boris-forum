/**
 * @namespace
 * @memberOf client
 */
const components = {
  App: require('App').default,
  Auth: require('Auth').default,
  Messagges: require('Messages').default
};
