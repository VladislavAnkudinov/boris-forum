import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import AutoComplete from 'material-ui/AutoComplete';

import * as actions_auth from '../actions/actions_auth'

/**
 * Component for authentication
 * @memberOf client.components
 * @param {object} props - Passed parameters
 * @param {boolean} props.authenticated - If user authenticated its true
 */
class Auth extends Component {
  /**
   * Executed before component actually mounted
   */
  componentWillMount() {
    if (localStorage.token) window.APP_HISTORY.push('/');
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.authenticated && !this.props.authenticated) window.APP_HISTORY.push('/');
  }

  signin() {
    console.log('SIGNIN!');
    this.props.postSignin({email: 'alice@example.com', password: 'wonderland'});
  };

  /**
   * Renders component to HTML
   * @returns {HTML}
   */
  render() {
    return (
      <div>
        <AutoComplete
          hintText='email'
          dataSource={['alice@example.com', 'bob@example.com']}
        />
        <br/>
        <AutoComplete
          hintText='password'
          dataSource={[]}
        />
        <br/>
        <RaisedButton label='Sign In' primary onClick={this.signin.bind(this)} />
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated,
    username: state.auth.username
  };
}

export default connect(mapStateToProps, {...actions_auth})(Auth);
