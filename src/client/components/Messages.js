import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';

import {List, ListItem} from 'material-ui/List';
import ContentInbox from 'material-ui/svg-icons/content/inbox';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import ContentSend from 'material-ui/svg-icons/content/send';
import ContentDrafts from 'material-ui/svg-icons/content/drafts';
import Divider from 'material-ui/Divider';
import ActionInfo from 'material-ui/svg-icons/action/info';

import RaisedButton from 'material-ui/RaisedButton';
import AutoComplete from 'material-ui/AutoComplete';

import * as actions_messages from '../actions/actions_messages';

/**
 * Component which contents messages
 * @param {object} props - Passed parameters
 * @param {boolean} props.authenticated - If user authenticated its true
 * @memberOf client.components
 */
class Messages extends Component {
  componentWillMount() {
    this.props.getMessages();
  }

  post() {
    this.props.postMessage({header: 'awesome header!', body: 'awewome body!'})
      .then(() => {
        this.props.getMessages();
      })
  };

  /**
   * Renders component to HTML
   * @returns {HTML}
   */
  render() {
    const listItems = (this.props.messages || []).map(message => (
      <ListItem primaryText={`author: ${message.author} header: ${message.header} body: ${message.body}`} />
    ));
    return (
      <div>
        <List>
          {listItems}
        </List>
        <div>
          <AutoComplete
            hintText='header'
            dataSource={[]}
          />
          <br/>
          <AutoComplete
            hintText='body'
            dataSource={[]}
          />
          <br/>
          <RaisedButton label='Post' primary onClick={this.post.bind(this)} />
        </div>
      </div>
    );
    /*
    return (
      <div>
        <List>
          <ListItem primaryText="Inbox" leftIcon={<ContentInbox />} />
          <ListItem primaryText="Starred" leftIcon={<ActionGrade />} />
          <ListItem primaryText="Sent mail" leftIcon={<ContentSend />} />
          <ListItem primaryText="Drafts" leftIcon={<ContentDrafts />} />
          <ListItem primaryText="Inbox" leftIcon={<ContentInbox />} />
        </List>
        <Divider />
        <List>
          <ListItem primaryText="All mail" rightIcon={<ActionInfo />} />
          <ListItem primaryText="Trash" rightIcon={<ActionInfo />} />
          <ListItem primaryText="Spam" rightIcon={<ActionInfo />} />
          <ListItem primaryText="Follow up" rightIcon={<ActionInfo />} />
        </List>
      </div>
    )
    */
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.auth.authenticated,
    username: state.auth.username,
    messages: state.messages.messages
  };
}

export default connect(mapStateToProps, {...actions_messages})(Messages);
