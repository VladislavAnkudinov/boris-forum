import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';

/**
 * Namespace for reducers
 * @namespace
 * @memberOf client
 */
const reducers = combineReducers({
  form,
  auth: require('./reducer_auth').default,
  messages: require('./reducer_messages').default
});

export default reducers;
