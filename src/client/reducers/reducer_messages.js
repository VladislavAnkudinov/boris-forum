import {
  POST_MESSAGE,
  GET_MESSAGES,
  GET_MESSAGE,
  PUT_MESSAGE,
  DELETE_MESSAGE
} from '../actions/aux/actions_types';

/**
 * Messages reducer
 * @memberOf client.reducers
 * @param {object} state - Old state
 * @param {object} action - Reducer action
 * @param {actions.aux.actions_types} action.type - Action type
 * @returns {object} - New state
 */
export default function (state = {}, action) {
  let newState;
  switch(action.type) {
    case POST_MESSAGE:
      newState = {...state};
      break;
    case GET_MESSAGES:
      newState = {...state, messages: action.payload};
      break;
    case GET_MESSAGE:
      newState = {...state};
      break;
    case PUT_MESSAGE:
      newState = {...state};
      break;
    case DELETE_MESSAGE:
      newState = {...state};
      break;
    default:
      newState = state;
  }
  return newState;
}
