import {
   AUTH_USER,
   UNAUTH_USER,
   AUTH_ERROR
 } from '../actions/aux/actions_types';

/**
 * Auth reducer
 * @memberOf client.reducers
 * @param {object} state - Old state
 * @param {object} action - Reducer action
 * @param {actions.aux.actions_types} action.type - Action type
 * @returns {object} - New state
 */
export default function (state = {}, action) {
  let newState;
  switch(action.type) {
    case AUTH_USER:
      newState = {...state, error: '', authenticated: true, username: action.username};
      break;
    case UNAUTH_USER:
      newState = {};
      break;
    case AUTH_ERROR:
      newState = {...state, error: action.error};
      break;
    default:
      newState = state;
  }
  return newState;
}
