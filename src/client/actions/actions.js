/**
 * Namespace for actions
 * @namespace
 * @memberOf client
 */
const actions = {
  /**
   * @namespace
   */
  aux: {
    /**
     * @namespace
     */
    actions_helpers: require('./aux/actions_helpers'),
    /**
     * @namespace
     */
    actions_types: require('./aux/actions_types')
  },
  /**
   * @namespace
   */
  auth: require('./actions_auth'),
  /**
   * @namespace
   */
  messages: require('./actions_messages')
};

export default actions;
