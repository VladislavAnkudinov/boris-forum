// auth
/**
 * Action type when user authentication
 * @memberOf actions.aux.actions_types
 * @type {string}
 */
export const AUTH_USER = 'AUTH_USER';

/**
 * Action type when user logout
 * @memberOf actions.aux.actions_types
 * @type {string}
 */
export const UNAUTH_USER = 'UNAUTH_USER';

/**
 * Action type when error while auth
 * @memberOf client.actions.aux.actions_types
 * @type {string}
 */
export const AUTH_ERROR = 'AUTH_ERROR';

// messages
/**
 * Action type when create new message
 * @memberOf client.actions.aux.actions_types
 * @type {string}
 */
export const POST_MESSAGE = 'POST_MESSAGE';

/**
 * Action type when obtain all messages id's
 * @memberOf client.actions.aux.actions_types
 * @type {string}
 */
export const GET_MESSAGES = 'GET_MESSAGES';

/**
 * Action type when get particular message
 * @memberOf client.actions.aux.actions_types
 * @type {string}
 */
export const GET_MESSAGE = 'GET_MESSAGE';

/**
 * Action type when particular message was edited
 * @memberOf client.actions.aux.actions_types
 * @type {string}
 */
export const PUT_MESSAGE = 'PUT_MESSAGE';

/**
 * Action type when particular message was deleted
 * @memberOf client.actions.aux.actions_types
 * @type {string}
 */
export const DELETE_MESSAGE = 'DELETE_MESSAGE';
