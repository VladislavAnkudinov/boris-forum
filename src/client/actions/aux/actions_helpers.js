/**
 * Path to main page from host
 * @memberOf client.actions.aux.actions_helpers
 * @type {any}
 */
export const BASE_URL = window.BASE_URL;

/**
 * Hostname
 * @memberOf client.actions.aux.actions_helpers
 * @type {string}
 */
export const LOCATION_HOSTNAME = window.location.hostname;

/**
 * Port
 * @memberOf client.actions.aux.actions_helpers
 * @type {string}
 */
export const LOCATION_PORT = window.location.port;

/**
 * Protocol
 * @memberOf client.actions.aux.actions_helpers
 * @type {string}
 */
export const LOCATION_PROTOCOL = window.location.protocol;

/**
 * Api url
 * @memberOf client.actions.aux.actions_helpers
 * @type {string}
 */
export const API_URL = `${LOCATION_PROTOCOL}//${LOCATION_HOSTNAME}:${LOCATION_PORT}${BASE_URL}api`;

window.API_URL_CONST = API_URL;
