
import axios from 'axios';

import {
  AUTH_USER,
  AUTH_ERROR,
  UNAUTH_USER
} from './aux/actions_types';

import { API_URL } from './aux/actions_helpers';

/**
 * Call api/v1/auth/post_signin REST api and return function which return reducer action as a dispatched promise
 * @memberOf client.actions.auth
 * @param {object} props - Properties
 * @param {string} props.email - User email
 * @param {string} props.password - User password
 * @returns {Function} Function which returns promise with dispatcher reducer action
 */
export function postSignin({email, password}) {
  return function (dispatch) {
    // Submit email/password to the server
    return axios.post(`${API_URL}/v1/auth/post_signin`, {email, password})
      .then(response => {
        const appHistory = window.APP_HISTORY;
        // - Sawe the JWT token
        localStorage.setItem('token', response.data.token);
        localStorage.setItem('username', email);
        // If request is good...
        // - Update state to indicate user is authenticated
        let o = {type: AUTH_USER, username: email};
        console.log(o);
        return dispatch(o);
        // - redirect to the route '/menu'
        //appHistory.push('/menu');
      })
      .catch(error => {
        // If requires is bad...
        // - Show an error to user
        return dispatch(authError('Bad login info'));
      });
  };
}

/**
 * Call api/v1/auth/post_signup REST api and return function which return reducer action as a dispatched promise
 * @memberOf client.actions.auth
 * @param {object} props - Properties
 * @param {string} props.email - User email
 * @param {string} props.password - User password
 * @returns {Function} Function which returns promise with dispatcher reducer action
 */
export function postSignup({email, password}) {
  return function (dispatch) {
    return axios.post(`${API_URL}/v1/auth/post_signup`, {email, password})
      .then(response => {
        const appHistory = window.APP_HISTORY;
        localStorage.setItem('token', response.data.token);
        localStorage.setItem('username', email);
        return dispatch({type: AUTH_USER, username: email});
        //appHistory.push('/messages');
      })
      .catch(response => {
        return dispatch(authError(response.data.error));
      });
  };
}

/**
 * Returns reducer action with auth error
 * @memberOf client.actions.auth
 * @param {Error} error - Auth error
 * @returns {{type: string, payload: *}}
 */
export function authError(error) {
  return {
    type: AUTH_ERROR,
    error
  };
}

/**
 * Delete auth tokens from localStorage and return reducer action
 * @memberOf client.actions.auth
 * @returns {{type: string, payload: {}}}
 * @returns.type {string}
 */
export function authSignout() {
  localStorage.removeItem('token');
  localStorage.removeItem('username');
  return {
    type: UNAUTH_USER,
  };
}
