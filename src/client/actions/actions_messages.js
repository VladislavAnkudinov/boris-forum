import axios from 'axios';

import {
  POST_MESSAGE,
  GET_MESSAGES,
  GET_MESSAGE,
  PUT_MESSAGE,
  DELETE_MESSAGE
} from './aux/actions_types';

import { API_URL } from './aux/actions_helpers';

/**
 * Call api/v1/messages/post_message REST api and return function which return reducer action as a dispatched promise
 * @memberOf clien.actions.messages
 * @param {object} props - Props
 * @param {string} props.header - Message header
 * @param {string} props.body - Message body
 * @returns {Function} Function which returns promise with dispatcher reducer action
 */
export function postMessage(props) {
  return function (dispatch) {
    // Submit email/password to the server
    return axios.post(`${API_URL}/v1/messages/post_message`, props, {
      headers: {
        authorization: localStorage.getItem('token'),
      }
    })
      .then(response => {
        return dispatch({type: POST_MESSAGE, payload: response.data});
      })
  };
}

/**
 * Call api/v1/messages/get_messages REST api and return function which return reducer action as a dispatched promise
 * @memberOf client.actions.messages
 * @returns {Function} Function which returns promise with dispatched reducer action
 */
export function getMessages() {
  return function (dispatch) {
    return axios.get(`${API_URL}/v1/messages/get_messages`, {
      headers: {
        authorization: localStorage.getItem('token'),
      }
    })
      .then(response => {
        return dispatch({type: GET_MESSAGES, payload: response.data});
      })
  };
}

/**
 * Call api/v1/messages/get_message REST api and return function which return reducer action as a dispatched promise
 * @memberOf client.actions.messages
 * @param {object} props - Props
 * @param {string} props._id - Message _id
 * @returns {Function} Function which returns promise with dispatched reducer action
 */
export function getMessage({_id}) {
  return function (dispatch) {
    return axios.post(`${API_URL}/v1/messages/get_message`, {
      headers: {
        authorization: localStorage.getItem('token'),
        _id
      }
    })
      .then(response => {
        return dispatch({type: GET_MESSAGE, payload: response.data});
      })
  };
}

/**
 * Call api/v1/messages/put_message REST api and return function which return reducer action as a dispatched promise
 * @memberOf client.actions.messages
 * @param {object} props - Props
 * @param {string} props._id - Message _id
 * @param {string | undefined} props.header - Message new header if updated
 * @param {string | undefined} props.body - Message new body if updated
 * @returns {Function} Function which returns promise with dispatcher reducer action
 */
export function putMessage(props) {
  let {_id} = props;
  delete props._id;
  return function (dispatch) {
    // Submit email/password to the server
    return axios.post(`${API_URL}/v1/messages/put_message`, props, {
      headers: {
        authorization: localStorage.getItem('token'),
        _id
      }
    })
      .then(response => {
        return dispatch({type: PUT_MESSAGE, payload: response.data});
      })
  };
}

/**
 * Call api/v1/messages/post_message REST api and return function which return reducer action as a dispatched promise
 * @memberOf client.actions.messages
 * @param {object} props - Props
 * @param {string} props._id - Message _id
 * @returns {Function} Function which returns promise with dispatcher reducer action
 */
export function deleteMessage({_id}) {
  return function (dispatch) {
    return axios.post(`${API_URL}/v1/messages/delete_message`, {
      headers: {
        authorization: localStorage.getItem('token'),
        _id
      }
    })
      .then(response => {
        return dispatch({type: DELETE_MESSAGE, payload: response.data});
      })
  };
}
