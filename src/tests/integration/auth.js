import 'dotenv/config';
import { expect } from 'chai';
import axios from 'axios';

const port = process.env.PORT || 3000;

describe('Integration tests for auth, it', () => {

  it('may auth as alice@example.com with right password', (done) => {
    axios.post(`http://localhost:${port}/api/v1/auth/post_signin`, {
      email: 'alice@example.com',
      password: 'wonderland'
    })
      .catch(res => {
        done(res.status);
      })
      .then(res => {
        expect(res.data).to.be.instanceOf(Object);
        expect(typeof res.data.token).to.equal('string');
        done();
      })
  });

  it('may auth as bob@example.com with right password', (done) => {
    axios.post(`http://localhost:${port}/api/v1/auth/post_signin`, {
      email: 'bob@example.com',
      password: 'glory'
    })
      .catch(res => {
        done(res.status);
      })
      .then(res => {
        expect(res.data).to.be.instanceOf(Object);
        expect(typeof res.data.token).to.equal('string');
        done();
      })
  });

  it('cant auth as alice@example.com with wrong password', (done) => {
    axios.post(`http://localhost:${port}/api/v1/auth/post_signin`, {
      email: 'alice@example.com',
      password: 'WRONG'
    })
      .then(() => {
        done(new Error('Can login with wrong password'));
      })
      .catch(res => {
        expect(res.status).to.equal(401);
        expect(res.statusText).to.equal('Unauthorized');
        done();
      })
  });

  it('cant auth as bob@example.com with wrong password', (done) => {
    axios.post(`http://localhost:${port}/api/v1/auth/post_signin`, {
      email: 'bob@example.com',
      password: 'WRONG'
    })
      .then(() => {
        done(new Error('Can login with wrong password'));
      })
      .catch(res => {
        expect(res.status).to.equal(401);
        expect(res.statusText).to.equal('Unauthorized');
        done();
      })
  });

});
