import 'dotenv/config';
import { expect } from 'chai';
import axios from 'axios';

const port = process.env.PORT || 3000;

describe('Integration tests for messages, it', () => {

  let token;
  let testMessageId;

  before((done) => {
    axios.post(`http://localhost:${port}/api/v1/auth/post_signin`, {
      email: 'alice@example.com',
      password: 'wonderland'
    })
      .catch(res => {
        done(res.status);
      })
      .then(res => {
        expect(res.data).to.be.instanceOf(Object);
        expect(typeof res.data.token).to.equal('string');
        token = res.data.token;
        done();
      })

  });

  it('can post message', (done) => {
    axios.post(`http://localhost:${port}/api/v1/messages/post_message`, {
      header: 'Test message header',
      body: 'Test message body'
    }, {
      headers: {
        authorization: token
      }
    })
      .catch(res => {
        throw new Error(res.data.error || res.status);
      })
      .then(res => {
        expect(res.data).to.be.instanceOf(Object);
        expect(typeof res.data._id).to.equal('string');
        testMessageId = res.data._id;
        done();
      })
      .catch(err => {
        done(err);
      })
  });

  it('can fetch all messages', (done) => {
    axios.get(`http://localhost:${port}/api/v1/messages/get_messages`, {
      headers: {
        authorization: token
      }
    })
      .catch(res => {
        throw new Error(res.data.error || res.status);
      })
      .then(res => {
        expect(res.data).to.be.instanceOf(Array);
        let idx = res.data.map(item => item._id).indexOf(testMessageId);
        expect(idx).to.not.equal(-1);
        expect(res.data[idx].author).to.equal('alice@example.com');
        expect(res.data[idx].header).to.equal('Test message header');
        done();
      })
      .catch(err => {
        done(err);
      })
  });

  it('can fetch particular message', (done) => {
    axios.get(`http://localhost:${port}/api/v1/messages/get_message`, {
      headers: {
        authorization: token,
        _id: testMessageId
      }
    })
      .catch(res => {
        throw new Error(res.data.error || res.status);
      })
      .then(res => {
        expect(res.data).to.be.instanceOf(Object);
        expect(res.data.author).to.equal('alice@example.com');
        expect(res.data.header).to.equal('Test message header');
        expect(res.data.body).to.equal('Test message body');
        done();
      })
      .catch(err => {
        done(err);
      })
  });

  it('can edit particular message', (done) => {
    axios.put(`http://localhost:${port}/api/v1/messages/put_message`, {
      header: 'Test new header',
      body: 'Test new body'
    }, {
      headers: {
        authorization: token,
        _id: testMessageId
      }
    })
      .catch(res => {
        throw new Error(res.data.error || res.status);
      })
      .then(res => {
        expect(res.data).to.be.instanceOf(Object);
        expect(res.data.oldMessage).to.be.instanceOf(Object);
        expect(res.data.newMessage).to.be.instanceOf(Object);
        expect(res.data.oldMessage.author).to.equal('alice@example.com');
        expect(res.data.oldMessage.header).to.equal('Test message header');
        expect(res.data.oldMessage.body).to.equal('Test message body');
        expect(res.data.newMessage.author).to.equal('alice@example.com');
        expect(res.data.newMessage.header).to.equal('Test new header');
        expect(res.data.newMessage.body).to.equal('Test new body');
        done();
      })
      .catch(err => {
        done(err);
      })
  });

  it('can delete message', (done) => {
    axios.delete(`http://localhost:${port}/api/v1/messages/delete_message`, {
      headers: {
        authorization: token,
        _id: testMessageId
      }
    })
      .catch(res => {
        throw new Error(res.data.error || res.status);
      })
      .then(res => {
        expect(res.data).to.be.instanceOf(Object);
        expect(typeof res.data._id).to.equal('string');
        expect(res.data._id).to.equal(testMessageId);
        expect(res.data.header).to.equal('Test new header');
        expect(res.data.body).to.equal('Test new body');
        done();
      })
      .catch(err => {
        done(err);
      })
  })

});
