import controllers from '../../server/controllers/controllers';
import { expect } from 'chai';

describe('Unit tests for controllers, they', () => {
  it('have auth namespace', () => {
    expect(controllers.auth).to.be.instanceOf(Object);
  });
  it('have post_signup at auth', () => {
    expect(controllers.auth.post_signup).to.be.instanceOf(Function);
  });
  it('have post_signin at auth', () => {
    expect(controllers.auth.post_signup).to.be.instanceOf(Function);
  });
  it('have messages namespace', () => {
    expect(controllers.messages).to.be.instanceOf(Object);
  });
  it('have post_message at messages', () => {
    expect(controllers.messages.post_message).to.be.instanceOf(Function);
  });
});
