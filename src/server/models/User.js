const log = require('bunyan').createLogger({name: 'server/models/User'});
const mongoose = require('mongoose');
const Promise = require('bluebird');
Promise.promisifyAll(mongoose);
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');
Promise.promisifyAll(bcrypt);

/**
 * user, mongoose model
 * @memberOf server.models
 */
let userSchema = new Schema({
  email: {type: String, unique: true, lowercase: true},
  password: String,
});

/**
 * Save method
 */
userSchema.pre('save', function (next) {
  bcrypt.genSaltAsync(10)
    .then((salt) => {
      return bcrypt.hashAsync(this.password, salt, null);
    })
    .then((hashed) => {
      this.password = hashed;
      return next();
    })
    .catch((err) => {
      err.type = 'user_pre_save';
      err.description = `User pre save error, email: ${this.email}`;
      log.warn(`
        Error!
          message: ${err.message}
          type: ${err.type}
          description: ${err.description}
      `);
      return next(err);
    });
});

/**
 * Compare password
 * @param candidatePassword
 * @param callback
 */
userSchema.methods.comparePassword = function (candidatePassword, callback) {
  bcrypt.compareAsync(candidatePassword, this.password)
    .then((isMatch) => {
      callback(null, isMatch);
    })
    .catch((err) => {
      err.type = 'bcrypt_compare_async';
      err.description = `Fail during bcrypt compare password of user, email: ${this.email}`;
      log.warn(`
        Error!
          message: ${err.message}
          type: ${err.type}
          description: ${err.description}
      `);
      callback(err);
    });
};

module.exports = mongoose.model('user', userSchema);

// this lines has no meaning, and they here just for WebStrom code checking,
// so that functions below not appear as 'unresolved function or method'
module.exports.findOneAsync = module.exports.findOneAsync || 'should contain promisified version of "findOne" function';
module.exports.saveAsync = module.exports.saveAsync || 'should contan promisified version of "save" function';
bcrypt.genSaltAsync = bcrypt.genSaltAsync || 'promisified "genSalt"';
bcrypt.hashAsync = bcrypt.hashAsync || 'promisified "hash"';
