const log = require('bunyan').createLogger({name: 'server/services/passport'});
const passport = require('passport');
const User = require('../models/User');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const LocalStrategy = require('passport-local');

const localOptions = {usernameField: 'email'};

const localLogin = new LocalStrategy(localOptions, (email, password, done) => {
  //User.findOneAsync({email: email})
  User.findOne({email: email})
    .then((user) => {
      if (!user) done(null, false);
      user.comparePassword(password, (err, isMatch) => {
        if (err) {
          err.type = 'user_compare_password';
          err.description = `Errored while comparing password of user, email: ${email}`;
          log.warn(`
            Error!
              message: ${err.message}
              type: ${err.type}
              description: ${err.description}
          `);
          return done(err, false);
        }
        if (!isMatch) return done(null, false);
        return done(null, user);
      });
    })
    .catch((err) => {
      err.type = 'user_find_one';
      err.description = `Errored while trying to find one user, email: ${email}`;
      log.warn(`
        Error!
          message: ${err.message}
          type: ${err.type}
          description: ${err.description}
      `);
      return done(err, false);
    });
});

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: process.env.SECRET
};

const jwtLogin = new JwtStrategy(jwtOptions, (payload, done) => {
  //User.findByIdAsync(payload.sub)
  User.findById(payload.sub)
    .then((user) => {
      //log.info('user =', user);
      if (user) {
        done(null, user);
      } else {
        done(null, false);
      }
    })
    .catch((err) => {
      err.type = 'user_find_by_id';
      err.description = `Errored while trying to find user, id: ${payload.sub}`;
      log.warn(`
        Error!
          message: ${err.message}
          type: ${err.type}
          description: ${err.description}
      `);
      done(err, false);
    });
});

passport.use(jwtLogin);
passport.use(localLogin);
