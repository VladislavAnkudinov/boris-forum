/**
 * @namespace server
 */

import 'source-map-support/register';
import 'dotenv/config';
const log = require('bunyan').createLogger({name: 'server/server'});
const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const bunyan = require('bunyan');
const mongoose = require('mongoose');
import router from './router/router';
const path = require('path');
const cors = require('cors');
const mongodb = require('mongodb');
const Promise = require('bluebird');
const serveIndex = require('serve-index');
const fs = require('fs');
Promise.promisifyAll(mongodb);

// DB setup
const MONGO_URI = process.env.MONGO_URI;
mongoose.connect(MONGO_URI);

// App setup
var app = new express();
app.mongoConnection = mongodb.MongoClient.connectAsync(MONGO_URI)
  .then(db => {
    log.info('Connected correctly to Server API mongo.');
    app.db = db;
    return db;
  })
  .catch(err => {
    log.warn(`
      Error while connecting to Server API mongo!
        message: ${err.message}
        type: ${err.type}
        description: ${err.description}
    `);
    throw err;
  });

app.use(morgan('combined'));
app.use(cors());

// serve static assets normally
app.use(express.static('public'));
//app.use('/jsdoc', serveIndex('jsdoc', {icons: true}));
app.use('/jsdoc', express.static('jsdoc'));
app.use('/fa', serveIndex('node_modules/font-awesome', {icons: true}));
app.use('/fa', express.static('node_modules/font-awesome'));
app.use('/project', serveIndex('./', {icons: true}));
app.use('/project', express.static('./'));
app.use(bodyParser.json({type: '*/*'}));
app.use((req, res, next) => {
  req.db = app.db;
  next();
});
router(app);

//handle every other route with index.html
app.get('*', function (req, res) {
  res.send(String(fs.readFileSync(path.resolve('templates', 'index.html'))));
});

// App start
const port = process.env.PORT || 3000;
const server = http.createServer(app);
app.mongoConnection.then(() => {
  server.listen(port, (err) => {
    if (err) {
      log.warn(`
      Error!
        message: ${err.message}
        type: ${err.type}
        description: ${err.description}
    `);
    } else {
      log.info('Server listening on port:', port);
    }
  });
});
