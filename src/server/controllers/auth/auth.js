import post_signin from './post_signin';
import post_signup from './post_signup';

const auth = {
  post_signin,
  post_signup
};

export default auth;
