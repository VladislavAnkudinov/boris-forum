const log = require('bunyan').createLogger({name: 'server/controllers/auth/post_signin'});
const User = require('../../models/User');
const jwt = require('jwt-simple');
const crypto = require('crypto');

function tokenForUser(user) {
  return jwt.encode({
    sub: user.id,
    iat: new Date().getTime,
    salt: crypto.randomBytes(8).toString('hex')
  }, process.env.SECRET);
}

/**
 * @name post_signin
 * @memberOf server.controllers.auth
 * @description Middleware to post signin
 * @function
 * @param {Request} req - Request object
 * @param {object} req.body - Request body
 * @param {string} req.body.email - User email
 * @param {string} req.body.password - User password
 * @param {Response} res - Response object
 * @param {Function} next - Go to next middleware
 */
export default function (req, res, next) {
  res.send({
    token: tokenForUser(req.user)
  });
}
