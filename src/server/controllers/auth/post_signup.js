const log = require('bunyan').createLogger({name: 'server/controllers/auth/post_signup'});
const User = require('../../models/User');
const jwt = require('jwt-simple');

function createErrorHandler(req, res, next) {
  return (err) => {
    log.warn(`
      Error!
        message: ${err.message}
        type: ${err.type}
        description: ${err.description}
    `);
    switch (err.type) {
      case 'wrong_data':
        res.status(422).send({error: err.message});
        break;
      case 'email_is_in_use':
        res.status(422).send({error: err.message});
        break;
      case 'user_find_one_async':
        res.status(500).send({error: 'Internal error'});
        break;
      default:
        res.status(500).send({error: 'Internal error'});
        break;
    }
  };
}

/**
 * @name post_signup
 * @memberOf server.controllers.auth
 * @description Middleware to post signup
 * @function
 * @param {Request} req - Request object
 * @param {object} req.body - Request body
 * @param {string} req.body.email - User email
 * @param {string} req.body.password - User password
 * @param {Response} res - Response object
 * @param {Function} next - Go to next middleware
 */
export default function (req, res, next) {
  let handleError = createErrorHandler(req, res, next);
  Promise.resolve(req.body)
    .then((body) => {
      if (!body.email || !body.password) {
        let err = new Error('You must provide email and password');
        err.type = 'wrong_data';
        err.description = `
          Wrong data - no email or password,
          email: ${body.email}
          password: ${body.password}
        `;
        throw err;
      }
      return User.findOneAsync({'email': req.body.email})
        .catch((err) => {
          err.type = 'user_find_one_async';
          err.description = `Cant find user by email: "${req.body.email}"`;
          throw err;
        });
    })
    .then((user) => {
      if (user) {
        let err = new Error('Email is in use');
        err.type = 'email_is_in_use';
        err.description = `Email "${req.body.email}" is in use`;
        throw err;
      }
      user = new User({
        email: req.body.email,
        password: req.body.password
      });
      return user.saveAsync();
    })
    .then((user) => {
      log.info(`
        User created!
          email: ${user.email}
          password: ${user.password}
      `);
      let doc = {...user._doc};
      delete doc.__v;
      delete doc.password;
      return res.send({...doc});
    })
    .catch(handleError);
};
