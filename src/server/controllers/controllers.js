import auth from './auth/auth';
import messages from './messages/messages';

/**
 * Controllers
 * @namespace
 * @memberOf server
 */
const controllers = {
  /**
   * Authentication controllers
   * @namespace
   */
  auth,
  /**
   * Messages controllers
   * @namespace
   */
  messages
};

export default controllers;
