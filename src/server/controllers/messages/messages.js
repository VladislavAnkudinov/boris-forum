import post_message from './post_message';
import get_messages from './get_messages';
import get_message from './get_message';
import put_message from './put_message';
import delete_message from './delete_message';

const messages = {
  post_message,
  get_messages,
  get_message,
  put_message,
  delete_message
};

export default messages;
