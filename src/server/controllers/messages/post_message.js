const log = require('bunyan').createLogger({name: 'server/controllers/messages/post_message'});
import { ObjectId } from 'mongodb';

/**
 * @name post_message
 * @memberOf server.controllers.messages
 * @description Middleware for creating new message
 * @function
 * @param {Request} req - Request object
 * @param {object} req.body - Request body
 * @param {string} req.body.header - Message header
 * @param {string} req.body.body - Message body
 * @param {Response} res - Response object
 * @param {Function} next - Go to next middleware
 */
export default function post_message(req, res, next) {
  let {header, body} = req.body;
  let author = req.user.email;
  let date = new Date();
  if (!header) return res.status(422).json({error: 'Message header should be provided'});
  if (typeof header !== 'string') return res.status(422).json({error: 'Message header wrong'});
  if (!body) return res.status(422).json({error: 'Message body should be provided'});
  if (typeof body !== 'string') return res.status(422).json({error: 'Message body wrong'});
  return req.db.collection('messages').insert({author, header, body, date})
    .then(result => {
      return res.json(result.ops[0]);
    })
    .catch(err => {
      log.warn('error =', err);
      return res.status(500).json({error: 'Internal error'});
    });
};
