const log = require('bunyan').createLogger({name: 'server/controllers/messages/get_messages'});
import { ObjectId } from 'mongodb';

/**
 * @name get_messages
 * @memberOf server.controllers.messages
 * @description Middleware for getting all messages
 * @function
 * @param {Request} req - Request object
 * @param {Response} res - Response object
 * @param {Function} next - Go to next middleware
 */
export default function get_messages(req, res, next) {
  return req.db.collection('messages').find({}).toArray()
    .then(messages => {
      return res.json([...messages]);
    })
    .catch(err => {
      log.warn('error =', err);
      return res.status(500).json({error: 'Internal error'});
    })
};
