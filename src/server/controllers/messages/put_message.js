const log = require('bunyan').createLogger({name: 'server/controllers/messages/put_message'});
import { ObjectId } from 'mongodb';

/**
 * @name put_message
 * @memberOf server.controllers.messages
 * @description Middleware for editing particular message
 * @function
 * @param {Request} req - Request object
 * @param {object} req.header - Request header
 * @param {string} req.header._id - Message _id
 * @param {object} req.body - Request body
 * @param {string | undefined} req.body.header - Message new header if updated
 * @param {string | undefined} req.body.body - Message new body if updated
 * @param {Response} res - Response object
 * @param {Function} next - Go to next middleware
 */
export default function put_message(req, res, next) {
  let {_id} = req.headers;
  if (!_id) return res.status(422).json({error: 'Message _id should be provided (in header)'});
  try {
    _id = ObjectId(_id);
  } catch (err) {
    return res.status(422).json({error: 'Wrong message _id'})
  }
  let {header, body} = req.body;
  if (!header && !body) return res.status(422).json({error: 'Provide new body or header for message'});
  let $set = {date: new Date()};
  if (header) {
    if (typeof header !== 'string') return res.status(422).json({error: 'Message new header wrong'});
    $set.header = header;
  }
  if (body) {
    if (typeof body !== 'string') return res.status(422).json({error: 'Message new body wrong'});
    $set.body = body;
  }
  let oldMessage;
  let newMessage;
  return req.db.collection('messages').findOne({_id})
    .then(msg => {
      if (!msg) return res.status(400).json({error: 'Message not found'});
      oldMessage = msg;
      return req.db.collection('messages').update({_id}, {$set})
        .then(() => {
          return req.db.collection('messages').findOne({_id})
        })
        .then(msg => {
          newMessage = msg;
          return res.json({oldMessage, newMessage});
        })
    })
    .catch(err => {
      log.warn('error =', err);
      return res.status(500).json({error: 'Internal error'});
    });
};
