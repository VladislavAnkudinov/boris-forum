const log = require('bunyan').createLogger({name: 'server/controllers/messages/get_message'});
import { ObjectId } from 'mongodb';

/**
 * @name get_message
 * @memberOf server.controllers.messages
 * @description Middleware for getting particular message
 * @function
 * @param {Request} req - Request object
 * @param {object} req.headers - Request headers
 * @param {string} req.headers._id - Message _id
 * @param {Response} res - Response object
 * @param {Function} next - Go to next middleware
 */
export default function get_message(req, res, next) {
  let {_id} = req.headers;
  if (!_id) return res.status(422).json({error: 'Message _id should be provided (in header)'});
  try {
    _id = ObjectId(_id);
  } catch (err) {
    return res.status(422).json({error: 'Wrong message _id'})
  }
  return req.db.collection('messages').find({_id}).toArray()
    .then(messages => {
      return res.json(messages[0]);
    })
    .catch(err => {
      log.warn('error =', err);
      return res.json({
        error: err.message
      })
    })
};
