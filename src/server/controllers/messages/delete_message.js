const log = require('bunyan').createLogger({name: 'server/controllers/messages/delete_message'});
import { ObjectId } from 'mongodb';

const defaultHeaders = {
  host: 'localhost:3000',
  connection: 'keep-alive',
  'content-length': '59',
  'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',
  'postman-token': '6245b16c-c269-3956-7741-71ca90349a7e',
  'cache-control': 'no-cache',
  origin: 'chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop',
  'content-type': 'text/plain;charset=UTF-8',
  accept: '*/*',
  'accept-encoding': 'gzip, deflate, sdch',
  'accept-language': 'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4'
};

/**
 * @name delete_message
 * @memberOf server.controllers.messages
 * @description Middleware for deleting message
 * @function
 * @param {Request} req - Request object
 * @param {object} req.headers - Request headers
 * @param {string} req.headers._id - Message _id
 * @param {Response} res - Response object
 * @param {Function} next - Go to next middleware
 */
export default function delete_message(req, res, next) {
  let {_id} = req.headers;
  if (!_id) return res.status(422).json({error: 'Message _id should be provided (in header)'});
  try {
    _id = ObjectId(_id);
  } catch (err) {
    return res.status(422).json({error: 'Wrong message _id'})
  }
  return req.db.collection('messages').findOne({_id})
    .then(message => {
      if (!message) return res.status(400).json({error: 'Message not found'});
      return req.db.collection('messages').removeOne({_id})
        .then(() => {
          return res.json({...message});
        })
    })
    .catch(err => {
      log.warn('error =', err);
      return res.json({error: err.message})
    })
};
