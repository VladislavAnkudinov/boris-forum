const log = require('bunyan').createLogger({name: 'routes/aux/router_helpers'});

const passportService = require('../../services/passport');
const passport = require('passport');

const requireAuth = passport.authenticate('jwt', {session: false});
const requireSignin = passport.authenticate('local', {session: false});

export default function (app) {
  app.customHelpers = {
    requireAuth,
    requireSignin,
  };
};
