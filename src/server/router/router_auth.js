/**
 * @namespace auth
 * @memberOf server.router
 */

const log = require('bunyan').createLogger({name: 'routes/router_auth'});
import controllers from '../controllers/controllers';

/**
 * @typedef {object} UserIdEmail
 * @memberOf server.router.auth
 * @prop {string} _id - User _id
 * @prop {string} email - User email
 */

/**
 * @typedef {object} UserToken
 * @memberOf server.router.auth
 * @prop {string} token - User token
 */

export default function (app) {
  /**
   * @name POST /api/v1/auth/post_signup
   * @memberOf server.router.auth
   * @description Route to create user
   * @function
   * @param {object} body - Body of request, contains credentials
   * @param {string} body.email - New user email
   * @param {string} body.password - New user password
   * @returns {server.router.auth.UserIdEmail | server.router.Error} Created user or error
   */
  app.post('/api/v1/auth/post_signup',
    controllers.auth.post_signup);

  /**
   * @name POST /api/v1/auth/post_signin
   * @memberOf server.router.auth
   * @description Route to auth user
   * @function
   * @param {object} headers - Request headers
   * @param {string} headers.authorization - Authorization token
   * @param {object} body - Body of request, contains credentials
   * @param {string} body.email - User email
   * @param {string} body.password - User password
   * @returns {server.router.auth.UserToken | server.router.Error} User token or error
   */
  app.post('/api/v1/auth/post_signin',
    app.customHelpers.requireSignin,
    controllers.auth.post_signin);

};
