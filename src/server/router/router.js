/**
 * @namespace router
 * @memberOf server
 */

/**
 * @typedef {object} Error
 * @memberOf server.router
 * @prop {string} error - Error message
 */

const log = require('bunyan').createLogger({name: 'routes/router'});

import router_helpers from './aux/router_helpers';
import router_auth from './router_auth';
import router_messages from './router_messages';

export default function (app) {
  router_helpers(app);
  router_auth(app);
  router_messages(app);
  app.get('/api/v1/*',
    app.customHelpers.requireAuth,
    (req, res) => {
      res.send({
        kind: 'error',
        error: 'unknown_route'
      });
    });
};