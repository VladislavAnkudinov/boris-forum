/**
 * @namespace messages
 * @memberOf server.router
 */

const log = require('bunyan').createLogger({name: 'routes/router_messages'});
import controllers from '../controllers/controllers';

/**
 * @typedef {object} Message
 * @memberOf server.router.messages
 * @prop {string} _id - Message id
 * @prop {string} author - Message author (email)
 * @prop {string} header - Message header
 * @prop {string} body - Message body
 * @prop {Date} date - Message date
 */

/**
 * @typedef {object} MessageWithoutBody
 * @memberOf server.router.messages
 * @prop {string} _id - Message id
 * @prop {string} author - Message author (email)
 * @prop {string} header - Message header
 * @prop {Date} date - Message date
 */

/**
 * @typeof {object} MessageEdit
 * @memberOf server.router.messages
 * @prop {server.router.messages.Message} oldMessage - Old message
 * @prop {server.router.messages.Message} newMessage - New message
 */

export default function (app) {
  /**
   * @name POST /api/v1/messages/post_message
   * @memberOf server.router.messages
   * @description Route to create new message at database
   * @function
   * @param {object} headers - Headers of request
   * @param {string} headers.authorization - Authorization token
   * @param {object} body - Body of request, contains props of new message
   * @param {string} body.header - Message header
   * @param {string} body.body - Message body
   * @returns {server.router.messages.Message | server.router.Error} Posted message or error
   */
  app.post('/api/v1/messages/post_message',
    app.customHelpers.requireAuth,
    controllers.messages.post_message);

  /**
   * @name GET /api/v1/messages/get_messages
   * @memberOf server.router.messages
   * @description Route to retreive all messages without bodies
   * @function
   * @param {object} headers - Headers of request
   * @param {string} headers.authorization - Authorization token
   * @returns {server.router.messages.MessageWithoutBody[] | server.router.Error} All messages without bodies or error
   */
  app.get('/api/v1/messages/get_messages',
    app.customHelpers.requireAuth,
    controllers.messages.get_messages);

  /**
   * @name GET /api/v1/messages/get_message
   * @memberOf server.router.messages
   * @description Route to get particular message
   * @function
   * @param {object} headers - Headers of request
   * @param {string} headers.authorization - Authorization token
   * @param {string} headers.message_id - Message id
   * @returns {server.router.messages.Message | server.router.Error} Message or error
   */
  app.get('/api/v1/messages/get_message',
    app.customHelpers.requireAuth,
    controllers.messages.get_message);

  /**
   * @name PUT /api/v1/messages/put_message
   * @memberOf server.router.messages
   * @description Route to edit particular message
   * @function
   * @param {object} headers - Headers of request
   * @param {string} headers.authorization - Authorization token
   * @param {string} headers.message_id - Message id
   * @param {object} body - Body of request, new props message
   * @param {string | undefined} body.header - Message new header if updated
   * @param {string | undefined} body.body - Message new body if updated
   * @returns {server.router.messages.MessageEdit | server.router.Error} Old and new messages or error
   */
  app.put('/api/v1/messages/put_message',
    app.customHelpers.requireAuth,
    controllers.messages.put_message);

  /**
   * @name DELETE /api/v1/messages/delete_message
   * @memberOf server.router.messages
   * @description Route to delete particular message
   * @function
   * @param {object} headers - Headers of request
   * @param {string} headers.authorization - Authorization token
   * @param {string} headers.message_id - Message id
   * @returns {server.router.messages.Message | server.router.Error} Deleted message or error
   */
  app.delete('/api/v1/messages/delete_message',
    app.customHelpers.requireAuth,
    controllers.messages.delete_message);
};
