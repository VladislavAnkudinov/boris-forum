# Boris Forum

_Boris Forum_ is the simple web application for demonstration tasks.
It prowides simple frontend and backend for posting and reading messages

## Explore
To quckly explore _Boris Forum_ navigate your browser to http://178.62.210.6/ and try the following users to authenticate

 - username: `alice@example.com`, password: `wonderland`
 - username: `bob@example.com`, password: `glory`

## Installation, build and run
 If you want to install and build everything by yourself do the following

 - clone repo (or copy, downlowd, etc..)
 - run `npm install`
 - run `npm run build`
 - create `.env` file and copy contents of `.env-example` file into it
 - adjust `.env` file according to your configurations
 - fire `npm start`
 - voila!
 
 All dependencies has to be installed using npm

## Documentation
To browse documentation go to http://178.62.210.6/jsdoc or 

 - run server
 - navigate your browser to `localhost:<PORT>/jsdoc` e.g. http://localhost:3000/jsdoc

Documetation has to be generated from comments with jsdoc, if you want to do so by yourself

 - run `npm jsdoc` from the root of the project
 
## Tests

#### Manual

 For manual testing there are two users:
 
 - username: `alice@example.com`, password: `wonderland`
 - username: `bob@example.com`, password: `glory`

#### Integration

 - may cause data
 - may take a lot of time

 to run integration tests
 
 - fire `npm run integration`

#### Unit

 - don't cause any data
 - should be fast

 to run unit tests
 
 - fire `npm run unit`

## Development

 - fire `npm run watch`

It will build and start the server and rebuild and restart it every time when
changes occur in sources of the server. 
It will also rebuild client if any changes happen, dont forget to refresh the page then.

## License

private © Vladislav Ankudinov 2016

```
    ##  ##
    ##  ##
#############        #############
  ###        ###     #############
  ###         ##     ###
  ###        ###     ###
  ###########        ########
  ############       ########
  ###         ###    ###
  ###          ##    ###
  ###         ###    ###
###############      ###
#############        ###
    ##  ##
    ##  ##
```
