var webpack = require('webpack');
var path = require('path');
var fs = require('fs');

var plugins = [];
var NODE_ENV = process.env.NODE_ENV;
if (NODE_ENV == 'production') {
  console.log('PRODUCTION BUILD');
  plugins = [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.DefinePlugin({
      '__DEVTOOLS__': false //set it to true in dev mode
    }),
    //new webpack.optimize.CommonsChunkPlugin('common.js'),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({minimize: true}),
    new webpack.optimize.AggressiveMergingPlugin()
  ];
} else {
  console.log('DEVELOPMENT BUILD');
}

var BLD_FLD = 'bld';
var ASSETS_FLD = path.join('public', 'assets');

var deleteFolderRecursive = function (path) {
  if (fs.existsSync(path)) {
    fs.readdirSync(path).forEach(function (file, index) {
      var curPath = path + '/' + file;
      if (fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
};

// Delete everything on the first start
deleteFolderRecursive(BLD_FLD);
deleteFolderRecursive(ASSETS_FLD);

var nodeModules = {};
var bundleModules = [
  //'three'
  'radium'
];
fs.readdirSync('node_modules')
  .filter(function (x) {
    return ['.bin'].indexOf(x) === -1;
  })
  .filter(function (x) {
    return bundleModules.indexOf(x) === -1;
  })
  .forEach(function (mod) {
    //console.log('not bundle module =', mod);
    nodeModules[mod] = 'commonjs ' + mod;
  });

var jobModule = {
  loaders: [
    {
      test: /\.js$/,
      loader: 'babel',
      query: {
        presets: ['es2015', 'stage-0']
      }
    },
    {
      loader: 'json-loader',
      test: /\.json$/
    }
  ]
};

var jobs = [];

var testsBuilder = function (testsType) {
  return fs.readdirSync(path.join('src', 'tests', testsType))
    .map(function (filename) {
      return {
        target: 'node',
        //devtool: 'source-map',
        entry: ['babel-polyfill', './' + path.join('src', 'tests', testsType, filename)],
        output: {
          path: path.join(__dirname, BLD_FLD, testsType),
          filename: filename
        },
        module: jobModule,
        externals: nodeModules,
        //plugins: plugins
      };
    });
};

jobs = [].concat(jobs, testsBuilder('integration'));
jobs = [].concat(jobs, testsBuilder('unit'));

jobs.push({
  target: 'web',
  //devtool: 'source-map',
  entry: [process.env.NODE_ENV == 'production' ? 'babel-polyfill/dist/polyfill.min' : 'babel-polyfill/dist/polyfill'
    , './' + path.join('src', 'client', 'client.js')],
  output: {
    path: path.join(__dirname, ASSETS_FLD),
    filename: 'client.js'
  },
  module: jobModule,
  //externals: nodeModules,
  plugins: plugins
});

jobs.push({
  target: 'node',
  //devtool: 'source-map',
  entry: ['babel-polyfill', './' + path.join('src', 'server', 'server.js')],
  output: {
    path: path.join(__dirname, BLD_FLD),
    filename: 'server-boris-forum.js'
  },
  module: jobModule,
  externals: nodeModules,
  //plugins: plugins
});

module.exports = jobs;
